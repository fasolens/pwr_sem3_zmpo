//
// Created by fasolens on 13.01.18.
//

#include "Sorting.hpp"

bool bubbleSort(std::vector<Book> &a, bool (*comparator)(Book &, Book &)) {
    bool swapped; // flag for optimization
    int temp; // helping variable for setting last swap position
    int end = a.size() - 1; // position of last swapping for optimization

    for (int i = 0; i < a.size(); i++) {
        swapped = false;
        for (int j = 0; j < end; j++) {
            if (!comparator(a[j], a[j + 1])) { // comparator return true if two object is sorted ascending
                swapped = true;
                temp = j;
                std::swap(a[j], a[j + 1]); // swap two values
            }
        }
        end = temp;
        if (!swapped) break; // if false break the loop no more swapping to do
    }
    return true; // if sorted
}