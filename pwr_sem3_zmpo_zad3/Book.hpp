//
// Created by fasolens on 12/15/17.
//

#ifndef PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP
#define PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP


#include <string>
#include <iostream>

class Book {
protected:
    std::string tytul;
    std::string autor;
    int liczba_stron;
public:
    static unsigned copy;
    static unsigned move;
    static unsigned constr;
    Book() {
        this->tytul = "";
        this->autor = "";
        this->liczba_stron = 0;
        constr++;
    }
    Book(char const* tytul, char const* autor, int l_stron) {
        this->tytul = tytul;
        this->autor = autor;
        this->liczba_stron = l_stron;
        constr++;
    }
    Book(const Book &book)
        : tytul(book.tytul),
          autor(book.autor),
          liczba_stron(book.liczba_stron)
    {
       copy++;
    }
    Book(Book &&book)
         : tytul(book.tytul),
           autor(book.autor),
           liczba_stron(book.liczba_stron)
    {
        book.tytul = "";
        book.autor = "";
        book.liczba_stron = 0;
        move++;
    }
    ~Book() {}
    Book&operator=(const Book& book) {
        if (this != &book) {
            // free existing

            // copy
            tytul = book.tytul;
            autor = book.autor;
            liczba_stron = book.liczba_stron;
            copy++;
        }
        return *this;
    }
    Book&operator=(Book&& book) {
        if (this != &book) {
            // free existing

            // copy
            tytul = book.tytul;
            autor = book.autor;
            liczba_stron = book.liczba_stron;

            // release
            book.tytul = "";
            book.autor = "";
            book.liczba_stron = 0;
            move++;
        }
        return *this;
    }
    void set_liczba_stron(int ls) {
       liczba_stron = ls;
    }
    void change_tytul(std::string s) {
        tytul = s;
    }
    void print_info() {
        std::cout << "\nKsiążka:\n" << tytul << std::endl;
        std::cout << autor << std::endl;
        std::cout << "liczba stron: " << liczba_stron << std::endl;
    }
    const std::string get_autor() {
        return autor;
    }
    std::string get_tytul() {
        return tytul;
    }
    int get_l_stron() {
        return liczba_stron;
    }
};


#endif //PWR_SEM3_ZMPO_ZAD3_1_BOOK_HPP
