//
// Created by fasolens on 13.01.18.
//

#ifndef PWR_SEM3_ZMPO_ZAD3_1_SORTING_HPP
#define PWR_SEM3_ZMPO_ZAD3_1_SORTING_HPP

#include <vector>
#include "Book.hpp"

bool bubbleSort(std::vector<Book> &a, bool (*comparator)(Book &, Book &));

#endif //PWR_SEM3_ZMPO_ZAD3_1_SORTING_HPP
