#include <iostream>
#include <vector>
#include <algorithm>
#include "Book.hpp"
#include "Sorting.hpp"

unsigned Book::copy = 0;
unsigned Book::move = 0;
unsigned Book::constr = 0;

//
//struct s_autor {
//    inline bool operator() (Book& b1, Book& b2) {
//        return (b1.get_autor() < b2.get_autor());
//    }
//};
//struct s_tytul {
//    inline bool operator() (Book& b1, Book& b2) {
//        return (b1.get_tytul() < b2.get_tytul());
//    }
//};
//struct s_l_stron {
//    inline bool operator() (Book& b1, Book& b2) {
//        return (b1.get_l_stron() < b2.get_l_stron());
//    }
//};

bool autor_comparator(Book &left, Book &right) {
    return left.get_autor()<right.get_autor();
}

bool tytul_comparator(Book &left, Book &right) {
    return  left.get_tytul()<right.get_tytul();
}

bool l_stron_comparator(Book &left, Book &right) {
    return left.get_l_stron() < right.get_l_stron();
}

int main() {
    Book b1 = Book("W pustynie i puszczy", "Jakiś dziad", 1234);
    b1.print_info();
    Book b2(b1);
    b2.print_info();
    b2.set_liczba_stron(1200);
    b2.change_tytul("Krzyżacy");
    b1.print_info();
    b2.print_info();

    Book b3 = Book("Anemik", "Dwa dziady", 345);
    b3.print_info();
    Book b4(std::move(b3));
    b4.print_info();
    b4.set_liczba_stron(1200);
    b4.change_tytul("Krzyżacy");
    b3.print_info();
    b4.print_info();
    std::cout << b1.constr << std::endl << b1.copy << std::endl << b1.move;

    std::vector <Book> books;
    books.push_back(Book("Władca Pierścieni", "Tolkien", 1300));
    books.push_back(Book("Ucieczka Nathana", "Grsham", 238));
    books.push_back(Book("Pasja C+", "Grębosz", 500));
    books.push_back(Book("Zanurkuj w Pythonie", "Pilgrim", 354));
    books.push_back(Book("Pan Lodowego Ogrodu", "Grzędowicz", 1121));
    books.push_back(Book("Zielona Mila", "King", 156));
    books.push_back(Book("Alicja w krainie czarów", "Carroll", 437));
    books.push_back(Book("Mistrz i Małgorzata", "Bułhakow", 421));
    books.push_back(Book("Toksyna", "Cook", 395));
    books.push_back(Book("Wiedźmin", "Sapkowski", 954));

    for (int i = 0; i < books.size(); ++i) {
        books[i].print_info();
    }
    std::cout << "### Sortuj po autorze #####################";
//    std::sort(books.begin(), books.end(), s_autor());
    bubbleSort(books, autor_comparator);
    for (int i = 0; i < books.size(); ++i) {
        books[i].print_info();
    }
    std::cout << "### sortuj po tytule #####################";
//    std::sort(books.begin(), books.end(), s_tytul());
    bubbleSort(books, tytul_comparator);
    for (int i = 0; i < books.size(); ++i) {
        books[i].print_info();
    }
    std::cout << "### sortuj po liczbie stron #####################";
//    std::sort(books.begin(), books.end(), s_l_stron());
    bubbleSort(books, l_stron_comparator);
    for (int i = 0; i < books.size(); ++i) {
        books[i].print_info();
    }
    std::cout << "########################";
    std::cout << std::endl << "Wywołania konstruktora: " << b1.constr
              << std::endl << "Wywołania kopiującego: " <<b1.copy
              << std::endl << "Wywołanie przenoszącego: " << b1.move;
    return 0;
}