#include <iostream>
#include "mymath.h"

int main() {
    int x, y;
    std::cout << "Podaj liczbę: ";
    std::cin >> x;
    std::cout << "Podaj wielkość potęgi: ";
    std::cin >> y;
    try {
        unsigned int wynik = potega(x, y);
        std::cout << "Wynik działania " << x << "^" << y << " wynosi: " << wynik << std::endl;
    } catch (std::string e) {
        std::cout << e;
    }

    return 0;
}
