#include "mymath.h"

unsigned int potega(int x, int y) throw(std::string) {
    if (x < 0) {
        std::string wyjatek = "Liczby mniejsze od zera nie są obsługiwane";
        throw wyjatek;
    } else if (x==0)
        return 0;
    if (y == 0)
        return 1;
    else if (y == 1)
        return x;
    else if (y < 0) {
        std::string wyjatek = "Liczby w potędze mniejsze od zera nie są obsługiwane";
        throw wyjatek;
    } else {
        unsigned int result = x * potega(x, y - 1);
        if (result == 0) {
            std::string wyjatek = "Wynik przekroczył zakres obsługiwanych liczb. Max=4'294'967'295";
            throw wyjatek;
        }
        return result;
    }
}
