#include <iostream>
#include <vector>

#define MYSQR1(x) x * x
#define MYSQR2(x) x * (x)

int f(int& x) {
    return ++x;
}
int x = 5, y = 6;
int main() {

//    ZADANIE 1
//    std::cout << MYSQR1(x + 3) << " " << MYSQR1((x + 3)) << " ";
//    // 5+3*5+3 = 5+15+3 = 23
//    // (5+3)*(5+3) = 8*8 = 64
//    std::cout << MYSQR2(x - 2) << " " << MYSQR2( f(x) ) << " " << x << std::endl;
//    // 5-2*(5-2) = 5-2*3 = 5-6 = -1
//    // 5->6 -> 6*7=42                     5
//
//    std::vector<int> v1;
//    v1.push_back(3);
//    v1.push_back(5);
//    v1.insert(v1.begin() + 1, 7);
//    v1.insert(v1.begin() + 2, 10);
//    v1.insert(v1.erase(v1.end() - 1), 16);
//    v1.insert(v1.end() - 1, 13);
////    v1.insert(v1.begin() + 3, (v1.begin() + 3)[1]);
//    for (int i = 0; i < v1.size(); i++) {
//        std::cout << v1[i] << " ";
//    }
//    std::cout << std::endl;
//
//    ZADANIE 2
//    int tab[10] = {1,2,3,4,5,6,7,8,9,10};
//    int *t = new int[10];
//
//    std::cout << sizeof(tab) << " " << sizeof(t) << " ";
//    std::cout << sizeof(tab[1] + 3.5) << std::endl;
//    std::cout << sizeof(tab[1]) << std::endl;
//    t = tab + 3;
//    std::cout << *t << " ";
//    std::cout << *(++t -2) << " ";
//    std::cout << t - tab << " " << *((&tab[4]) + 2) << std::endl;

//    {class X {
//        public:
//            int *p;
//            int s;
//            X(int s=1) {
//                std::cout << "N";
//                this->s = s;
//                p = new int[s];
//            };
//            X(const X &rs) {
//                std::cout << "C";
//                p = new int[s = rs.s];
//            };
//            ~X() {
//                std::cout << "D";
//            }
////            malloc!!
//        };
//        {
////            X *t = (X*)malloc(4 *)
////            t[0] = X(1); t[1] = X
//            X o = t[0];
//            X *tt = new X[2];
//        }
//
//    }
////    virtual classes
//    class B {
//    public:
//        virtual void print() { std::cout << "0 ";};
//        void printB() { std::cout << "B";}
//    };
//    class C : public B {
//    public:
//        virtual void print() { std::cout << "7 ";};
//    };
//    class P1 : public B, C {
//    public:
//        virtual void print() { std::cout << "1 ";};
//        void printP1() { std::cout << "X ";};
//    };
//    class P2 : public B {
//    public:
//        virtual void print() { std::cout << "2 ";};
//        void printP2() { std::cout << "Y ";};
//    };
//    class c1 {
//    public:
//        int x;
//        c1() {x=12;}
//        c1(int a) { x = a;};
//    };
//    class c2 : virtual public c1 {};
//    class c3 : virtual private c1 {};
//    class c4 : public c2, public c3 {
//
//    };
//    c1 c = c4();
//    std::cout << c.x;
//    class A {
//    public:
//        void virtual fun() {
//            std::cout << "AA";
//        };
//    };
//    class B : public A {
//    private:
//        void fun() {
//            std::cout << "BB";
//        };
//    };
//    A* wsk;
//    B elem_klasy_B;
//    wsk = &elem_klasy_B;
//    wsk -> fun();
    // A7 A

    class figura {

    };
    class trojkat : public figura {

    };
    try {
        trojkat rownor = trojkat();
        throw rownor;
    } catch (figura jakas) {
        std::cout << "A1";
    } catch (trojkat jakis) {
        std::cout << "B1";
    }
    return 0;
}