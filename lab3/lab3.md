1) Proszę napisać klasę Vehicle, dziedziczące o niej klasy Car oraz Boat. 0.5p
2) Proszę napisać klasę Amphibian, dziedziczącą po klasie Car oraz Boat. 1.0p

W zadaniu tym istotne jest zaprezentowanie takich technik programistycznych
jak enkapsulacja, klasy abstrakcyjne, dziedziczenie (wielokrotne, wirtualne)
oraz polimorfizm.

Klasy te następnie należy przystosować do następującego zadania:

Mamy zadaną pewną liczbę punktów, ułożonych w sekwencję. Dla każdej pary sąsiednich
punktów mamy podaną odległość do kolejnego punktu drogą lądową oraz drogą wodną.

3) Wygeneruj losowo taką sekwencję punktów. 0.5p
4) Utwórz przynajmniej 3 instancje Vehicle reprezentujące różne klasy. 0.5p
5) Dla każdej instancji policz najkrótszą ścieżkę od początku do końca sekwencji. 0.5p