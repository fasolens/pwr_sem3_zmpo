#include "TripPoint.h"
#include "Amphibian.h"
////
//// Created by fasolens on 25.11.17.
////
//
//#include "Amphibian.h"
//float Amphibian::go_to_destination(TripPoint point) {
//  return Car::go_to_destination(point);
//}
////Amphibian::Amphibian(char *name) : Vehicle(name) {
////
////}
float Amphibian::go(TripPoint point) {
    float shorter;
    if (point.next_by_boat < point.next_by_car) {
        shorter = point.next_by_boat;
    } else {
        shorter = point.next_by_car;
    }
    return shorter;
}
