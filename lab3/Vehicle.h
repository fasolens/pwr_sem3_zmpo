//
// Created by fasolens on 25.11.17.
//

#ifndef LAB3_VEHICLE_H
#define LAB3_VEHICLE_H

#include <iostream>
#include "TripPoint.h"

class Vehicle {
public:
    Vehicle() {}
    Vehicle(char const* brand): m_brand(brand) {}
    virtual ~Vehicle() {}
    virtual float go(TripPoint point) = 0;
    std::string getName() {return m_brand;}

protected:
    std::string m_brand;
};

#endif //LAB3_VEHICLE_H
