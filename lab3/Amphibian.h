//
// Created by fasolens on 25.11.17.
//

#ifndef LAB3_AMPHIBIAN_H
#define LAB3_AMPHIBIAN_H

#include <cmath>
#include "Car.h"
#include "Boat.h"

class Amphibian : public virtual Car, public virtual Boat {
public:
    Amphibian();
    Amphibian(char const* name): Vehicle(name) {}
    float go(TripPoint point) override;
};

#endif //LAB3_AMPHIBIAN_H
