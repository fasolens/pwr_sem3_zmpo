//
// Created by fasolens on 25.11.17.
//

#ifndef LAB3_CAR_H
#define LAB3_CAR_H

#include "Vehicle.h"
class Car : public virtual Vehicle {
public:
    Car();
    Car(char const* name): Vehicle(name) {}
    ~Car();
    float go(TripPoint point) override;
};

#endif //LAB3_CAR_H
