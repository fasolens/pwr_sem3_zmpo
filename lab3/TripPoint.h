//
// Created by fasolens on 25.11.17.
//

#ifndef LAB3_POINT_H
#define LAB3_POINT_H
class TripPoint {
 public:
  float next_by_car;
  float next_by_boat;
  TripPoint();
};

#endif //LAB3_POINT_H
