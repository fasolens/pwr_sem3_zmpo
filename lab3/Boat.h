//
// Created by fasolens on 25.11.17.
//

#ifndef LAB3_BOAT_H
#define LAB3_BOAT_H

#include "Vehicle.h"
class Boat : public virtual Vehicle {
public:
    Boat();
    Boat(char const* name): Vehicle(name) {}
    ~Boat();
    float go(TripPoint point) override;
};

#endif //LAB3_BOAT_H
