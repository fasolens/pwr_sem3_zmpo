#include <iostream>
#include "TripPoint.h"
#include "Car.h"
#include "Boat.h"
#include "Amphibian.h"
int main() {
  int numberPoints = 10;
  TripPoint listPoints[numberPoints];

  for (int i = 0; i < numberPoints; ++i) {
    listPoints[i] = TripPoint();
  }
    Car car = Car("auto");
    Boat boat = Boat("łódź");
    Amphibian amphibian = Amphibian("Amfibia");
  for (int i = 0; i < numberPoints; ++i) {
      std::cout << "GROUND " << listPoints[i].next_by_car << std::endl;
      std::cout << "WATER " << listPoints[i].next_by_boat << std::endl;
      std::cout << "\tAuto: " << car.go(listPoints[i]) << std::endl;
      std::cout << "\tŁódź: " << boat.go(listPoints[i]) << std::endl;
      std::cout << "\tAmfibia " << amphibian.go(listPoints[i]) << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
  }
  return 0;
}