Ostrzegam, jeśli jakieś zadanie wydaje się zbyt proste to prawdopodobnie czegoś nie rozumiecie - zastanówcie się zanim gafę strzelicie.
Podpowiedź: w miejscu gdzie proszę was o 4 konstruktory liczba ta nie jest przypadkowa - jest ku temu powód.

3) Proszę zaimplementować jedną z następujących struktur danych ozywając tablicy w języku C++
	- stos													0.50 punktów
	lub
	- kolejka cykliczna										0.75 punktów

UWAGA: jeśli ktoś nie wie co to jest kolejka cykliczna - jest to kolejka zrealizowana w 
    tablicy która nie przesuwa elementów po usunięciu elementu.

Zamiast tego tablica wykorzystywana jest cyklicznie.
	Uwagi do zadania:
	- realizacja jako struct lub class						(obowiązkowe)
	- realizacja z dynamiczną alokacją pamięci				0.25 punktów
	- poprawna implementacja (czterech) konstruktorów		0.75 punktów
	- poprawna implementacja destruktora					0.25 punktów
	- funkcja zwraca informację, że element nie może być
	  dodany/usunięty										0.25 punktów