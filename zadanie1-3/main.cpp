#include <iostream>
#include "kolejka_cykliczna.h"

int main() {
  try {
    kolejka_cykliczna <int> queue1(5);
    queue1.show();
    queue1.add(4);
    queue1.add(5);
    queue1.add(6);
    queue1.add(7);
    queue1.add(8);
    queue1.show();

    queue1.add(9);
    queue1.add(10);
    std::cout << "Kolejka 1: " << std::endl;
    queue1.show();
    kolejka_cykliczna <int> queue2(queue1);
    std::cout << "Nowa Kolejka 2 utworzona konstr. kopiujacym: " << std::endl;
    queue2.show();

    kolejka_cykliczna <int> queue3(std::move(queue2));

    std::cout << std::endl << "Pokazanie przeniesionej kolejki. Wskaznik jest na null wiec nic nie pokaze." << std::endl;
    queue2.show();

    std::cout << std::endl << "Wybieranie kolejnych elementow z kolejki 1 az do momentu gdy jest pusta." << std::endl;
    std::cout << queue1.pop() << "\t";
    std::cout << queue1.pop() << "\t";
    std::cout << queue1.pop() << "\t";
    std::cout << queue1.pop() << "\t";
    std::cout << queue1.pop() << std::endl;
    std::cout << std::endl << "Próba zdjęcia kolejnego elementu gdy kolejka jest pusta." << std::endl;
    queue1.pop();
  } catch (std::exception& e) {
    std::cout << "Standard exception: " << e.what() << std::endl;
  }
  return 0;
}
