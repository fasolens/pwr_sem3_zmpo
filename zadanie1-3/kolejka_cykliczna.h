#ifndef ZADANIE1_3_KOLEJKA_CYKLICZNA_H
#define ZADANIE1_3_KOLEJKA_CYKLICZNA_H

#include <iostream>

template <class T>
class kolejka_cykliczna {
public:
  kolejka_cykliczna();
  kolejka_cykliczna(int size);
  kolejka_cykliczna(const kolejka_cykliczna &kolejka);
  kolejka_cykliczna(kolejka_cykliczna &&kolejka);
  ~kolejka_cykliczna();
  void add(T x);
  T pop();
  void show();
private:
  int size;
  int current_size;
  int tail;
  int head;
  T* queue;
};

template <class T>
kolejka_cykliczna<T>::kolejka_cykliczna() {
  std::cout << "Podaj wielkość kolejki: ";
  std::cin >> this->size;
  this->current_size = 0;
  this->tail = 0;
  this->head = 0;
  queue = new T[size];
  for (int i = 0; i < size; ++i) {
    queue[i] = 0;
  }
}

template <class T>
kolejka_cykliczna<T>::kolejka_cykliczna(int size) {
  this->current_size = 0;
  this->tail = 0;
  this->head = 0;
  this->size = size;
  queue = new T[size];
  for (int i = 0; i < size; ++i) {
    queue[i] = 0;
  }
}

template <class T>
kolejka_cykliczna<T>::kolejka_cykliczna(const kolejka_cykliczna &kolejka) {
  this->current_size = kolejka.current_size;
  this->tail = kolejka.tail;
  this->head = kolejka.head;
  this->size = kolejka.size;
  queue = new T[size];
  for (int i = 0; i < size; ++i) {
    this->queue[i] = kolejka.queue[i];
  }
}

template <class T>
kolejka_cykliczna<T>::kolejka_cykliczna(kolejka_cykliczna &&kolejka) {
  this->current_size = kolejka.current_size;
  this->tail = kolejka.tail;
  this->head = kolejka.head;
  this->size = kolejka.size;
  queue = kolejka.queue;
  kolejka.queue = nullptr;
  kolejka.head = 0;
  kolejka.tail = 0;
  kolejka.current_size = 0;
}

template <class T>
kolejka_cykliczna<T>::~kolejka_cykliczna() {
  if (queue != NULL)
    delete [] queue;
}

template <class T>
void kolejka_cykliczna<T>::add(T x) {
  if (current_size < size) {
    if (tail < size) {
      queue[tail++] = x;
    } else {
      tail = 0;
      queue[tail++] = x;
    }
    current_size++;
  } else {
    if (tail < size) {
      queue[tail++] = x;
      head++;
    } else {
      tail = 0;
      queue[tail++] = x;
      head++;
    }
  }
}

template <class T>
T kolejka_cykliczna<T>::pop() {
  if (queue != NULL) {
    T result = 0;
    if (current_size <= 0) {
      throw std::runtime_error(std::string("Kolejka pusta."));
    } else {
      result = queue[head++];
      if (head >= size)
        head = 0;
      current_size--;
//      std::cout << result << std::endl;
    }
    return result;
  }
}

template <class T>
void kolejka_cykliczna<T>::show() {
  if (current_size == 0) {
    std::cout << "Kolejka jest pusta" << std::endl;
  } else {
    for (int i = 0; i < size; ++i) {
      std::cout << queue[i] << "\t";
    }
    std::cout << std::endl;
  }
}

#endif //ZADANIE1_3_KOLEJKA_CYKLICZNA_H
