//
// Created by fasolens on 03.01.18.
//

#ifndef PWR_SEM3_ZMPO_ZAD4_RATIONAL_NUMBER_H
#define PWR_SEM3_ZMPO_ZAD4_RATIONAL_NUMBER_H

/**
 * Nie ma potrzeby pisać konstruktora kopiowania oraz operatora kopiowania. Domyślne zachowanie kompilatora
 * jest wystarczające. Przekopiuje ono pokolei każdą składową klasy.
 *
 * Konstruktor oraz operator przenoszenia również nie są potrzebne. Dane zostałą skopiowane do nowego obiektu.
 * Jako że klasa nie posiada żadnych wskaźników to pamięć zostanie wyczyszczona przez kompilator.
 *
 * Operatory arytmetyczne oraz porównania zostały przeze mnie przeciążone.
 * Operator I/O również, gdyż możemy podać wartość prosto z konsoli przy użyciu `std::cin >> `
 *
 * Zaimplementowane kontruktory to
 * domyślny który zwraca ustawia liczbę na 0/1
 * jednym parametrem (int)
 * z dwoma parametrami (int, int)
 * oraz z jednym parametrem (std::string)
 */

class RationalNumber {
public:
    RationalNumber();
    RationalNumber(int);
    RationalNumber(int, int);
    RationalNumber(std::string);

    // Operators
    RationalNumber operator+(const RationalNumber& rationalNumber);
    RationalNumber&operator+=(const RationalNumber& rationalNumber);
    RationalNumber operator-(const RationalNumber& rationalNumber);
    RationalNumber&operator-=(const RationalNumber& rationalNumber);
    RationalNumber operator*(const RationalNumber& rationalNumber);
    RationalNumber&operator*=(const RationalNumber& rationalNumber);
    RationalNumber operator/(const RationalNumber& rationalNumber);
    RationalNumber&operator/=(const RationalNumber& rationalNumber);
    bool operator<(const RationalNumber& rationalNumber);
    bool operator>(const RationalNumber& rationalNumber);
    bool operator==(const RationalNumber& rationalNumber);
    bool operator!=(const RationalNumber& rationalNumber);
    bool operator<=(const RationalNumber& rationalNumber);
    bool operator>=(const RationalNumber& rationalNumber);

    friend std::ostream&operator<<(std::ostream& stream, const RationalNumber& rationalNumber);
    friend std::istream&operator>>(std::istream& stream, RationalNumber& rationalNumber);

    // Copy constructor ?
//    RationalNumber(const RationalNumber& rationalNumber);
//    RationalNumber&operator=(const RationalNumber& rationalNumber);

    // Move constructor ?
//    RationalNumber(RationalNumber&& rationalNumber);
//    RationalNumber&operator=(RationalNumber&& rationalNumber);

    // Destructor ?
private:
    int numerator;
    int denominator;
    void normalize(int, int);
};

#endif //PWR_SEM3_ZMPO_ZAD4_RATIONAL_NUMBER_H
