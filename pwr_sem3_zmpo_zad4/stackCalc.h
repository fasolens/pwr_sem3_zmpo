//
// Created by fasolens on 1/11/18.
//

#ifndef PWR_SEM3_ZMPO_ZAD4_STACKCALC_H
#define PWR_SEM3_ZMPO_ZAD4_STACKCALC_H


#include "Stack.h"
#include <sstream>

template <class T>
class StackCalc {
private:
    Stack<T> stack;
    std::string eq;
public:
    StackCalc() {
        eq = "";
        count(eq);
    }

    StackCalc(std::string q) {
        eq = q;
        count(eq);
    }

    void count() {
        count("");
    }

    void count(std::string givenEQ) {
        stack = Stack<T>();
        T l, r;
        T converted;

        std::string token;
        std::stringstream ss;
        std::istringstream toConvert;

        do {
            int charCode;
            ss.clear();
            ss.str(givenEQ);

            while(std::getline(ss, token, ' ')) {
                if (token.size()==0)
                    charCode = (int)token[0];
                else
                    charCode = (int)token[token.size()-1];
                if (charCode==42 || charCode==43 || charCode==45 || charCode==47) {
                    // arithmetic operators
                    if(stack.size() < 2) {
                        throw std::invalid_argument("Less than two arguments before operator.");
                    } else {
                        r = stack.pop();
                        l = stack.pop();
                        switch (charCode) {
                            case 42: // *
                                stack.push(l * r);
                                break;
                            case 43: // +
                                stack.push(l + r);
                                break;
                            case 45: // -
                                stack.push(l - r);
                                break;
                            case 47: // /
                                stack.push(l / r);
                                break;
                        }
                    }
                } else if(charCode >= 48 && charCode <= 57) {
                    toConvert.str(token);
                    toConvert >> converted;
                    stack.push(converted);
                    toConvert.clear();
                } else if(charCode == 61) {
                    std::cout << stack.pop();
                    return;
                } else {
                    throw std::invalid_argument("Invalid argument: " + token);
                }
            }
            std::cout << "> ";
            getline(std::cin, givenEQ);
        } while(givenEQ != "\\");
    }
};

#endif //PWR_SEM3_ZMPO_ZAD4_STACKCALC_H
