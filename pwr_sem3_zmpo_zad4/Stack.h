//
// Created by fasolens on 1/12/18.
//

#ifndef PWR_SEM3_ZMPO_ZAD4_STACK_H
#define PWR_SEM3_ZMPO_ZAD4_STACK_H


#include <vector>
#include <iostream>
#include "rationalNumber.h"

template <class T>
class Stack {
private:
    std::vector<T> stck;
    bool isEmpty() {
        return this->size() == 0;
    };
public:
    void push(T value) {
        this->stck.push_back(value);
    };
    T pop() {
        if (this->isEmpty()) {
            throw std::out_of_range("Stack is empty!");
        }
        T r = this->stck.back();
        this->stck.pop_back();
        return r;
    };
    unsigned long size() {
        return this->stck.size();
    };
    void show() {
        for (int i = 0; i < stck.size(); ++i) {
            std::cout << stck[i] << " ";
        }
        std::cout << std::endl;
    };
};

#endif //PWR_SEM3_ZMPO_ZAD4_STACK_H
