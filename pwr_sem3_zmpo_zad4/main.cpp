#include <iostream>
#include <vector>
#include "rationalNumber.h"
#include "stackCalc.h"

int main() {


    RationalNumber r = RationalNumber(-1,-1);
    std::cout << r << std::endl;
//    RationalNumber r = RationalNumber(1, 5);
    try {
        RationalNumber r1 = RationalNumber(-2, 1);
        RationalNumber r2 = RationalNumber(r1);
        RationalNumber r3 = RationalNumber(244, 14);
        r2=std::move(r3);
        std::cout << r1 << std::endl;
        std::cout << r2 << std::endl;
        std::cout << r3 << std::endl;
        r1 = RationalNumber(12, 11);
        r2 = RationalNumber(5, 12);
        r3 = r1+r2;
        std::cout << r3 << std::endl;
        r3 += r2;
        std::cout << r3 << std::endl;
        r3 = RationalNumber(13, 14) - RationalNumber(2,7);
        std::cout << r3 << std::endl;
        r3 -= r2;
        std::cout << r3 << std::endl;
        r3 = RationalNumber(5, 12) * RationalNumber(9, 14);
        std::cout << r3 << std::endl;
        r3 *= RationalNumber(3 ,5);
        std::cout << r3 << std::endl;
        r3 = RationalNumber(6, 11) / RationalNumber(12, 13);
        std::cout << r3 << std::endl;
        r3 = RationalNumber(9, 56);
        r3 /= RationalNumber(12, 13);
        std::cout << r3 << std::endl;
        std::cout << (RationalNumber(61, 100) < RationalNumber(71, 99)) << std::endl;
        std::cout << (RationalNumber(99, 100) > RationalNumber(71, 99)) << std::endl;
        std::cout << (RationalNumber(60, 100) <= RationalNumber(30, 50)) << std::endl;
        std::cout << (RationalNumber(60, 100) == RationalNumber(30, 50)) << std::endl;
        std::cout << (RationalNumber(20, 100) == RationalNumber(30, 50)) << std::endl;
        std::cout << (RationalNumber(60, 100) != RationalNumber(30, 50)) << std::endl;
        std::cout << (RationalNumber(20, 100) != RationalNumber(30, 50)) << std::endl;
        std::cout << (RationalNumber(61, 100) <= RationalNumber(71, 99)) << std::endl;
        std::cout << (RationalNumber(99, 100) >= RationalNumber(71, 99)) << std::endl;
        std::cout << (RationalNumber(62, 100) >= RationalNumber(31, 50)) << std::endl;
//        std::cout << "Podaj liczbę: ";
//        RationalNumber r4;
//        std::cin >> r4;
//        std::cout << r4 << std::endl;
    } catch(const std::runtime_error& e) {
        std::cout << e.what() << std::endl;
    }

    std::string eq = "15/3 7 1 1 + - / 3 * 2 1 1 + + - =";
    // 5 7 2 - / 3 * 2 1 1 + + - =
    // 5 5 / 3 * 2 1 1 + + - =
    // 1 3 * 2 1 1 + + - =
    // 3 2 1 1 + + - =
    // 3 2 2 + - =
    // 3 4 - =
    // -1 =
    // -1

    std::cout << "Witaj w kalkulatorze opartym na odwrotnej notacji polskiej." << std::endl;
    std::cout << "Jaki typ danych chesz użyć:" << std::endl << "1. int";
    std::cout << std::endl << "2. double";
    std::cout << std::endl << "3. liczby wymierne" << std::endl;
    std::string choice;
    std::cin >> choice;
    int ch = std::stoi(choice);
    switch (ch) {
        case 1:
            StackCalc<int>();
            break;
        case 2:
            StackCalc<double>();
            break;
        case 3:
            StackCalc<RationalNumber>("15/4 -7 1 1 + - / 3 * 2 1 -13 + + - =");
            break;
    }
//    exit(0);

//    StackCalc<RationalNumber> calc = StackCalc<RationalNumber>();
//
    return 0;
}