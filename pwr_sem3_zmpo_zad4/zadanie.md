Uwaga: Przypominam, że wymagane jest tylko uzyskanie 2 punktów.
Wykonanie zadań ponad 2 punkty idzie w kierunku poprawienia swojej oceny i podlega MOJEJ OSOBISTEJ OCENIE.
Nic nie jest gwarantowane i będzie zależało od waszej indywidualnej sytuacji.
Tego zadanie nie przyjmuję w postaci mailowej, zadanie można oddać 13 tylko i wyłącznie.

Sekcja A:
Proszę zaimplementować klasę liczb wymiernych 
(i.e. liczb które wyrażają się w postaci a/b gdzie a i b są liczbami całkowitymi).
1) Poprawna implementacja bazy klasy
    - konstruktory/destruktory/operatory przypisania... co tutaj jest potrzebne? (0.5 punkta)
DONE
2) Implementacja operacji arytmetycznych oraz porównań. (0.5 p)
DONE
3) Operacja wejścia/wyjścia na strumień. (0.5 p)
DONE
4) Unit testy wszystkich operacji. (0.5 p)
DONE
UWAGA: wybór operatorów czy operacji do zaimplementowania także podlega ocenie
- tj. jesteście oceniania z punktu widzenia tego, 
  czy wiecie czy trzeba/powinno się implementować w tej sytuacji np.: destruktor?
DONE
5) Uzasadnij pisemnie swoje decyzje (max: 20 zdań). (1 p)
DONE

Sekcja B:
Proszę zaimplementować tzw "Stack calculator". 
Co to jest Stack calculator? 
Kalkulator który liczy wyrażenia w oparciu o sekwencję 
operatorów (+, -, *, /, co jeszcze?) 
oraz literałów (2, 7, etc.) np:
wejście: 3
wejście: 4
wejście: +
  wyjście: 7
wejście: 2
wejście: /
  wyjście: 3.5

Wyrażenia te podawane są w kolejności zgodnej z tzw odwróconą notacją polską.
1) WYMAGANE: Dynamiczny stos własnego autorstwa. (0.5 p)
2) Wsparcie dla wyjątków (stdexcept). (0.5 p)
3) Program pracuje na liczbach wymiernych z sekcji A. (0.5 p)
4) Interfejs w konsoli dla programu, sprawdzanie błędów (np.: ile to jest marchewka 9 -?). (0.5 p)
5) Implementacja powyższego jako template 
  (musi działać minimalnie dla typu z sekcji A oraz typów double oraz int). (1 p)