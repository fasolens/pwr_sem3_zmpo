//
// Created by fasolens on 03.01.18.
//

#include <stdexcept>
#include <iostream>
#include "rationalNumber.h"


int gdc(int a, int b) {
    while(b) {
        int tmp = a;
        a = b;
        b = tmp%b;
    }
    return a;
}

int lcm(int a, int b) {
    return (a*b/gdc(a,b));
}



std::ostream &operator<<(std::ostream &stream, const RationalNumber &rationalNumber) {
    if (rationalNumber.denominator == 1) {
        stream << rationalNumber.numerator;
    } else {
        stream << rationalNumber.numerator << "/" << rationalNumber.denominator;
    }
    return stream;
}

std::istream &operator>>(std::istream &stream, RationalNumber &rationalNumber) {
    std::string str;
    stream >> str;
    rationalNumber=std::move(RationalNumber(str));
    return stream;
}

void RationalNumber::normalize(int num, int denom) {
    int g = gdc(num, denom);
    numerator = num/g;
    denominator = denom/g;
    if (numerator>0 && denominator<0) {
        numerator = -numerator;
        denominator = -denominator;
    }
}

RationalNumber::RationalNumber() {
    numerator = 0;
    denominator = 1;
}

RationalNumber::RationalNumber(int num) {
    numerator = num;
    denominator = 1;
}

RationalNumber::RationalNumber(int num , int denom) {
    if (denom == 0) {
        throw std::runtime_error("Divide by zero exception");
    }
    normalize(num, denom);
}

RationalNumber::RationalNumber(std::string str) {
    std::string s_numerator = "";
    std::string s_denominator = "";
    bool slash_flag = false;
    bool minus = false;
    for (int i = 0; i < str.size(); ++i) {
        char cc = str[i];
        if (std::isdigit(cc)) {
            if (!slash_flag) {
                s_numerator += cc;
            } else {
                s_denominator += cc;
            }
        } else if (cc == '/') {
            slash_flag = true;
        } else if (cc == '-') {
            minus = !minus;
        } else {
            throw std::invalid_argument("Format of number is incorrect");
        }
    }
    if (s_denominator == "") {
        s_denominator = "1";
    }
    if (minus) {
        *this = RationalNumber(-std::stoi(s_numerator), std::stoi(s_denominator));
    } else {
        *this = RationalNumber(std::stoi(s_numerator), std::stoi(s_denominator));
    }
}

RationalNumber RationalNumber::operator+(const RationalNumber &rationalNumber) {
    int denom = lcm(this->denominator, rationalNumber.denominator);
    int numerator = (this->numerator*denom/this->denominator) +
        (rationalNumber.numerator*denom/rationalNumber.denominator);
    return RationalNumber(numerator, denom);
}

RationalNumber &RationalNumber::operator+=(const RationalNumber &rationalNumber) {
    int denom = lcm(this->denominator, rationalNumber.denominator);
    int numerator = (this->numerator*denom/this->denominator) +
        (rationalNumber.numerator*denom/rationalNumber.denominator);
    normalize(numerator, denom);
    return *this;
}

RationalNumber RationalNumber::operator-(const RationalNumber &rationalNumber) {
    int denom = lcm(this->denominator, rationalNumber.denominator);
    int numerator = (this->numerator*denom/this->denominator) -
        (rationalNumber.numerator*denom/rationalNumber.denominator);
    return RationalNumber(numerator, denom);
}

RationalNumber &RationalNumber::operator-=(const RationalNumber &rationalNumber) {
    int denom = lcm(this->denominator, rationalNumber.denominator);
    int numerator = (this->numerator*denom/this->denominator) -
        (rationalNumber.numerator*denom/rationalNumber.denominator);
    normalize(numerator, denom);
    return *this;
}

RationalNumber RationalNumber::operator*(const RationalNumber &rationalNumber) {
    return RationalNumber(this->numerator*rationalNumber.numerator, this->denominator*rationalNumber.denominator);
}

RationalNumber &RationalNumber::operator*=(const RationalNumber &rationalNumber) {
    int denom = this->denominator*rationalNumber.denominator;
    int numerator = this->numerator*rationalNumber.numerator;
    normalize(numerator, denom);
    return *this;
}

RationalNumber RationalNumber::operator/(const RationalNumber &rationalNumber) {
    return RationalNumber(this->numerator*rationalNumber.denominator, this->denominator*rationalNumber.numerator);
}

RationalNumber &RationalNumber::operator/=(const RationalNumber &rationalNumber) {
    normalize(this->numerator*rationalNumber.denominator, this->denominator*rationalNumber.numerator);
    return *this;
}

bool RationalNumber::operator<(const RationalNumber &rationalNumber) {
    int common_denom = lcm(this->denominator, rationalNumber.denominator);
    return (this->numerator*common_denom/this->denominator < rationalNumber.numerator*common_denom/rationalNumber.denominator);
}

bool RationalNumber::operator>(const RationalNumber &rationalNumber) {
    RationalNumber r = RationalNumber(rationalNumber);
    return (r < *this);
}

bool RationalNumber::operator==(const RationalNumber &rationalNumber) {
    int common_denom = lcm(this->denominator, rationalNumber.denominator);
    return (this->numerator*common_denom/this->denominator == rationalNumber.numerator*common_denom/rationalNumber.denominator);
}

bool RationalNumber::operator!=(const RationalNumber &rationalNumber) {
    RationalNumber r = RationalNumber(rationalNumber);
    return !(r == *this);
}

bool RationalNumber::operator<=(const RationalNumber &rationalNumber) {
    RationalNumber r = RationalNumber(rationalNumber);
    return (*this < r || *this == r);
}

bool RationalNumber::operator>=(const RationalNumber &rationalNumber) {
    RationalNumber r = RationalNumber(rationalNumber);
    return (*this > r || *this == r);
}

//RationalNumber::RationalNumber(const RationalNumber& rationalNumber) {
//    numerator = rationalNumber.numerator;
//    denominator = rationalNumber.denominator;
//}
//
//RationalNumber &RationalNumber::operator=(const RationalNumber& rationalNumber) {
//    numerator = rationalNumber.numerator;
//    denominator = rationalNumber.denominator;
//    return *this;
//}
//
//RationalNumber::RationalNumber(RationalNumber &&rationalNumber) {
//    numerator = rationalNumber.numerator;
//    denominator = rationalNumber.denominator;
//    rationalNumber.numerator = 0;
//    rationalNumber.denominator = 1;
//}
//
//RationalNumber &RationalNumber::operator=(RationalNumber &&rationalNumber) {
//    numerator = rationalNumber.numerator;
//    denominator = rationalNumber.denominator;
//    rationalNumber.numerator = 0;
//    rationalNumber.denominator = 1;
//    return *this;
//}
