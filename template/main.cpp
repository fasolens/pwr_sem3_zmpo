#include <iostream>
#include "TempTest.h"

int main() {
  TempTest<int> A(23);
  TempTest<int> B(12, 34);
  A.show();
  B.show();
  return 0;
}