#include <iostream>

#ifndef TEMPLATE_TEMP_TEST_H
#define TEMPLATE_TEMP_TEST_H

template <class T>
class TempTest {
 public:
  T value1;
  T value2;
  TempTest(T v1);
  TempTest(T v1, T v2);
  void show();
};

template <class T> TempTest<T>::TempTest(T v1): value1(v1) {
  value2 = value1;
}

template <class T> TempTest<T>::TempTest(T v1, T v2): value1(v1), value2(v2) {
}

template <typename T>
void TempTest<T>::show() {
  std::cout << value1 << " " << value2;
}


#endif //TEMPLATE_TEMP_TEST_H
