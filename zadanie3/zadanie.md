Tytuł: zmpo - zad 3
Termin: 16.12.17

Zad 0:
(Dla osób które nie zdołały zrobić zadania na zajęciach lub osób które nie były na zajęciach - proszę wyjaśnić swoją sytuację jeśli przesyła się zadanie 0.) 1.5p
- Proszę napisać klasę abstrakcyjną Vehicle zawierającą nazwę pojazdu (zmienna, ustawiona w konstruktorze);
- Proszę napisać klasy Car i Boat dziedziczące po Vehicle;
- Proszę napisać klasę Amphibia dziedzicącą po Car i Boat;
- Proszę zaprezentować następujące techniki programistyczne:
	- dziedziczenie wirtualne,
	- enkapsulacja,
	- klasa abstrakcyjna,
	- polimorfizm (przez referencję lub przez wskaźnik),
	- poprawne wykorzystanie konstruktorów klas bazowych.

Zad 1:
Proszę napisać klasę Book, tytuł, autor, liczba stron.
Konstruktor parametryczny, kopiujący, przenoszący, operator = (kopiujący, przenoszący).
Statyczne wartości które liczą liczbę wywołań konstruktorów oraz operatorów (uwaga: konstruktor i operator kopiujący liczone razem, przenoszące też). 1.0p

Zad 2:
W oparciu o zadanie 1...
Funkcja sortuje std::vector według zadanego komparatora (FUNKCJA, NIE KLASA).
Trzy komparatory (sortują względem tytułu, autora, liczby stron).
Implementacja własna sortowania bąbelkowego.
Przeprowadzić sortowanie dla 10 książek, używając wszystkich trzech komparatorów.
Wypisać wartości zmiennych statycznych.

UWAGA 1: W rozwiązanie zadania ma być tylko 1 klasa.
UWAGA 2: Komparatory i sortowanie NIE SĄ częścią tej klasy.

Na przyszłych zajęciach zgłębiamy zagadnienie operatorów.
Proszę mieć ze sobą skończony kod do zadania domowego nr. 2!
