UWAGA: tytuł maila z odpowiedziami jest "ZMPO - zad 2".
Nie więcej, nie mniej i na pewno nie COŚ ZUPEŁNIE INNEGO.
W tytule nie dodajecie NIC. Tytuł jest taki jaki podałem i koniec.
Jest to prosta instrukcja i jako dorośli ludzi powinniście być w stanie się do niej zastosować.
Jeśli ktoś (znowu) prześle mi odpowiedź na zadanie z tytułem który jest inny niż powyższy po prostu nie sprawdzę tego zadania.

Termin: przed końcem dnia w którym mamy następne zajęcia (tj. do północy w sobotę).

Zadanie z zajęć
(Zadanie to można oddać za połową punktów (max 1) jeśli uzyskało się na lekcji mniej niż 1 punkt.
Tak czy inaczej sugeruję skończyć to zadanie bo ułatwicie sobie w ten sposób zadania z ostatniego tygodnia.
Jeśli ktoś jest usprawiedliwiony z swojej nieobecności może wykonać to zadanie na max punktów (2, punktacja jest podwojona), proszę przypomnieć mi o powodzie w mailu.)

1) Klasa macierz (nazwa własna jest dopuszczalna) 0.25p
- przechowujemy dane w postacie wskaźnika na wskaźnik
- alokacja dynamiczna przez konstruktor przyjmujący wymiary macierzy
- destruktor
2) Konstruktor kopiujący (copy) 0.25p
3) Konstruktor przemieszczający (move) 0.25p
4) Operacja dodawania na macierzy 0.25p

UWAGA: przypominam, że aby to wszystko działało jak trzeba należy ustawić swój kompilator w tryb C++11!

Zadanie domowe
(Zadanie dla każdego.)

1) Zawrzeć w kodzie w postaci komentarza wyjaśnienie do czego służy konstruktor kopiujący, konstruktor przemieszczający i jaka jest między nimi różnica, max 15 zdań w sumie. (1p)
2) Proszę dokończyć powyższa klasę i uzupełnić ją o następujące funkcje (uwaga: nie robimy operatorów)
- mnożenie macierzy 0.50p
- getter oraz setter do elementów 0.25p
- mnożenie przez skalar 0.25p
3) Proszę wszystkie napisane funkcje uzupełnić o obsługę wyjątków, zgodnie z bieżącymi standardami C++

Materiał do przygotowania na następne zajęcia:
1) Materiał dotychczasowy (konstruktory, etc.)
2) Dynamiczna alokacja pamięci na obiekty
3) Destruktory wirtualne
4) Dziedziczenie (prywatne, publiczne, wirtualne)