#ifndef LAB2_MATRIX_H
#define LAB2_MATRIX_H

class Matrix {
public:
    Matrix();
    Matrix(unsigned int rows, unsigned int cols);
    // konstruktor kopujący
    Matrix(const Matrix& fMatrix);
    Matrix&operator=(const Matrix& fMatrix);
    // konstruktor przenoszący
    Matrix(Matrix&& fMatrix);
    Matrix&operator=(Matrix&& fMatrix);
    ~Matrix();

    Matrix add(Matrix *m2) throw(std::runtime_error);
    Matrix operator+(const Matrix& fMatrix);
    Matrix&operator+=(const Matrix& fMatrix);
    void setElement(unsigned int r, unsigned int c, int value) throw(std::range_error);
    int getElement(unsigned int r, unsigned int c) throw(std::range_error);
    void multiple(int v);
    Matrix multiple(Matrix *m2) throw(std::runtime_error);
    Matrix operator*(const Matrix& fMatrix);
    Matrix&operator*=(const Matrix& fMatrix);
    Matrix operator*(const int skl);
    Matrix&operator*=(const int skl);
//    std::ostream&operator<<(std::ostream& s, const Matrix& fMatrix);
    void show();

private:
    unsigned int rows;
    unsigned int cols;
    int **matrix;
    void create();
    void fill();
};

#endif //LAB2_MATRIX_H
