#include <iostream>
#include "Matrix.h"

Matrix::Matrix() {
    this->rows = 1;
    this->cols = 1;
    create();
    fill();
}

Matrix::Matrix(unsigned int rows, unsigned int cols) : rows(rows), cols(cols) {
    create();
    fill();
}

Matrix::~Matrix() {
    if (matrix != NULL) {
        for (int i = 0; i < rows; ++i) {
            delete[] matrix[i];
        }
        delete[] matrix;
    }
}

/** Kontruktor kopijący stosujemy do "bezpiecznego" skopiowania objektu.
 * Gdy objekt posiada np. wskaźniki (Object *o;) to nie skopujemy wartości
 * wskaźniki z jednego objektu do drugiego, tylko przepiszemy wartości struktury
 * którą wskazuje ten wskaźnik. Dzięki temu unikamy sytuacji gdy na jednym adresie
 * pamięty operują dwa lub więcej obiektów.
*/
// Copy constructor
Matrix::Matrix(const Matrix &fMatrix) {
    rows = fMatrix.rows;
    cols = fMatrix.cols;
    create();
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            matrix[i][j] = fMatrix.matrix[i][j];
        }
    }
}
/** Operator kopiujący
 */
Matrix& Matrix::operator=(const Matrix& fMatrix) {
    if (this != &fMatrix) {
        // free existing data
        if (this->matrix != NULL) {
            for (int i = 0; i < this->rows; ++i) {
                delete[] this->matrix[i];
            }
            delete[] this->matrix;
        }

        // copy data
        this->rows = fMatrix.rows;
        this->cols = fMatrix.cols;
        this->create();
        for (int i = 0; i < this->rows; ++i) {
            for (int j = 0; j < this->cols; ++j) {
                this->matrix[i][j] = fMatrix.matrix[i][j];
            }
        }
    }
    return *this;
}
/** Kontruktor przenoszący służy do przeniesienia wszystkich "danych" objektu
 * przenoszonego do docelowego. Objekt przenoszony nie jest usuwany tylko
 * przywracany do "wartości domyślnych" (tak jakby został dopiero co utworzony).
 * W sytuacji o której mowa była przy konstruktorze kopjującym, adres wskaźnika
 * jest przepisywany do wskaźnika w nowym objekcie, a w objekcie przenoszonym jest
 * ustawiany na nullprt.
 */
// Move constructor
Matrix::Matrix(Matrix&& fMatrix) {
    rows = fMatrix.rows;
    cols = fMatrix.cols;
    matrix = fMatrix.matrix;
    fMatrix.matrix = nullptr;
}
/** Różnica pomiędzy kontruktorami, kopijącym i przenoszącym, polega na tym, że
 * kontruktor kopujący nie rusza objektu źródłowego, kontruktor przenoszący ustawia
 * jego parametry na domyślne.
 */

/** Operator przenoszący
 */
Matrix& Matrix::operator=(Matrix&& fMatrix) {
    if (this != &fMatrix) {
        // free existing
        if (this->matrix != NULL) {
            for (int i = 0; i < this->rows; ++i) {
                delete[] this->matrix[i];
            }
            delete[] this->matrix;
        }

        // copy data
        this->rows = fMatrix.rows;
        this->cols = fMatrix.cols;
        this->matrix = fMatrix.matrix;

        // release
        fMatrix.matrix = nullptr;
    }
    return *this;
}

Matrix Matrix::add(Matrix *m2) throw(std::runtime_error){
    try {
        if (rows!= m2->rows || cols!=m2->cols)
            throw std::runtime_error("Dimensions of matrix is not equal.");
        Matrix m(rows, cols);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                m.matrix[i][j] = matrix[i][j] + m2->matrix[i][j];
            }
        }
        return m;
    } catch (const std::runtime_error& ex) {
        std::cerr << ex.what() << std::endl;
    }
}
Matrix Matrix::operator+(const Matrix& fMatrix) {
//    try {
//        if (rows!= fMatrix.rows || cols != fMatrix.cols)
//            throw std::runtime_error("Dimensions of matrix is not equal.");
//        Matrix m(rows, cols);
//        for (int i = 0; i < rows; ++i) {
//            for (int j = 0; j < cols; ++j) {
//                m.matrix[i][j] = matrix[i][j] + fMatrix.matrix[i][j];
//            }
//        }
//        return m;
//    } catch (const std::runtime_error& ex) {
//        std::cerr << ex.what() << std::endl;
//    }
    return this->add(const_cast<Matrix *>(&fMatrix));
}
Matrix& Matrix::operator+=(const Matrix& fMatrix) {
    try {
        if (this->rows != fMatrix.rows || this->cols != fMatrix.cols)
            throw std::runtime_error("Dimensions of matrix is not equal.");
        for (int i = 0; i < this->rows; ++i) {
            for (int j = 0; j < this->cols; ++j) {
                this->matrix[i][j] += fMatrix.matrix[i][j];
            }
        }
        return *this;
    } catch (const std::runtime_error& ex) {
        std::cerr << ex.what() << std::endl;
    }
}

void Matrix::create() {
    try {
        matrix = new int *[rows];
        for (int i = 0; i < rows; ++i) {
            matrix[i] = new int[cols];
        }
    } catch (std::bad_alloc &ba) {
        std::cerr << "bad_alloc caught during creating matrix: " << ba.what() << '\n';
    }
}
void Matrix::show() {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            std::cout << matrix[i][j] << '\t';
        }
        std::cout << std::endl;
    }
}
void Matrix::fill() {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            matrix[i][j] = rand() % 10;
        }
    }
}
void Matrix::setElement(unsigned int r, unsigned int c, int value) throw(std::range_error) {
    try {
        if (r > rows || c > cols)
            throw std::range_error("Given value is out of range.");
        matrix[r][c] = value;
    } catch (const std::range_error& ex) {
        std::cerr << ex.what() << std::endl;
    }
}
int Matrix::getElement(unsigned int r, unsigned int c) throw(std::range_error) {
    try {
        if (r > rows || c > cols)
            throw std::range_error("Given value is out of range.");
        return matrix[r][c];
    } catch (const std::range_error& ex) {
        std::cerr << ex.what() << std::endl;
    }

}
void Matrix::multiple(int v) {
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            matrix[i][j] *= v;
        }
    }
}
Matrix Matrix::operator*(const int skl) {
    Matrix m(rows, cols);
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            m.matrix[i][j] = matrix[i][j] * skl;
        }
    }
    return m;
}
Matrix& Matrix::operator*=(const int skl) {
    this->multiple(skl);
    return *this;
}
Matrix Matrix::multiple(Matrix *m2) throw(std::runtime_error) {
    try {
        if (cols != m2->rows)
            throw std::runtime_error("Number of columns of 1st matrix is not equal numbers of rows of 2nd matrix.");
        Matrix m(rows, m2->cols);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < m2->cols; ++j) {
                m.matrix[i][j] = 0;
                for (int k = 0; k < cols; ++k) {
                    int mm1 = matrix[i][k];
                    int mm2 = m2->matrix[k][j];
                    m.matrix[i][j] += mm1 * mm2;
                }
            }
        }
        return m;
    } catch (const std::runtime_error& ex) {

    }
}
Matrix Matrix::operator*(const Matrix& fMatrix) {
    return this->multiple(const_cast<Matrix *>(&fMatrix));
}
Matrix& Matrix::operator*=(const Matrix& fMatrix) {
    try {
        if (cols != fMatrix.rows)
            throw std::runtime_error("Number of columns of 1st matrix is not equal numbers of rows of 2nd matrix.");
        Matrix m(rows, fMatrix.cols);
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < fMatrix.cols; ++j) {
                m.matrix[i][j] = 0;
                for (int k = 0; k < cols; ++k) {
                    int mm1 = matrix[i][k];
                    int mm2 = fMatrix.matrix[k][j];
                    m.matrix[i][j] += mm1 * mm2;
                }
            }
        }

        return *this;
    } catch (const std::runtime_error& ex) {

    }
}
//std::ostream& Matrix::operator<<(std::ostream& s, const Matrix& fMatrix) {
//    return s << '<' << v.x << ',' << v.y << '>';
//}
