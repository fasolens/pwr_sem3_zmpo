#include <iostream>
#include "Matrix.h"
int main() {
    Matrix *m1;
    Matrix *m2;
    Matrix *m3;
    m1 = new Matrix(2, 3);

    m2 = new Matrix(3, 2);

    m3 = new Matrix(3,2);

    std::cout << std::endl << "Maciez 1: " << std::endl;
    m1->setElement(0,0,1);
    m1->setElement(0,1,0);
    m1->setElement(0,2,2);
    m1->setElement(1,0,-1);
    m1->setElement(1,1,3);
    m1->setElement(1,2,1);
    m1->show();

    std::cout << std::endl << "Maciez 2: " << std::endl;
    m2->setElement(0,0,3);
    m2->setElement(0,1,1);
    m2->setElement(1,0,2);
    m2->setElement(1,1,1);
    m2->setElement(2,0,1);
    m2->setElement(2,1,0);
    m2->show();

    std::cout << std::endl << "Maciez 3: " << std::endl;
    m3->show();

    std::cout << std::endl << "Maciez wynikowa mnożenia:" << std::endl;
    Matrix m4(m1->multiple(m2));
    m4.show();

    std::cout << std::endl << "Maciez wynikowa x2:" << std::endl;
    m4.multiple(2);
    m4.show();

    std::cout << std::endl << "Maciez wynikowa dodania macierzy 2 do 3" << std::endl;
    Matrix *m6;
    m6 = new Matrix(m2->add(m3));
    m6->show();

    std::cout << std::endl << "Macież przypisana" << std::endl;
    m4=(*m6);
    m4.show();

    std::cout << std::endl << "Macież dodana przy użyciu operatora +" << std::endl;
    Matrix mm1 = m4+m4;
    mm1.show();

    std::cout << std::endl << "Macież dodana przy użyciu operatora +=" << std::endl;
    mm1+=m4;
    mm1.show();

    std::cout << std::endl << "Macież pomn. przez skalar operatorem *=" << std::endl;
    mm1*=3;
    mm1.show();

    std::cout << std::endl << "Macież pomn. przez skalar operatorem *" << std::endl;
    Matrix mm2 = mm1*2;
    mm2.show();
    return 0;
}